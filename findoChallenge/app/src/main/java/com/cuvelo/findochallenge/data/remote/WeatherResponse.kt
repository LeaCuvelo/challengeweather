package com.cuvelo.findochallenge.data.remote

data class WeatherResponse(
    val cod: Int,
    val coord: Coord,
    val main: Main,
    val name: String,
    val weather: List<Weather>,
    val sys: Sys
)

data class Sys(
    val country: String
)

data class Main(
    val humidity: Int,
    val pressure: Int,
    val temp: Double,
    val temp_max: Double,
    val temp_min: Double
)

data class Weather(
    val icon: String
)

data class Coord(
    val lat: Double,
    val lon: Double
)