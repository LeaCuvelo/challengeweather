package com.cuvelo.findochallenge.data.db

class RoomConfig {
    companion object {

        const val DATABASE_WEATHER = "weather.db"
        const val TABLE_CITIES= "cities"

        private const val SELECT_FROM = "SELECT * FROM "
        private const val LIMIT = " LIMIT 5"
        private const val ORDER_BY = " ORDER BY  "
        private const val DESC = "DESC"
        private const val TARGET_FIELD = "created_at "

        const val SELECT_CITIES = SELECT_FROM + TABLE_CITIES + ORDER_BY + TARGET_FIELD + DESC + LIMIT

    }
}