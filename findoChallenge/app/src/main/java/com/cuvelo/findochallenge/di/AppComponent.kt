package com.cuvelo.findochallenge.di

import com.cuvelo.findochallenge.ui.activity.MainActivity
import dagger.Component
import javax.inject.Singleton

@Component(modules = arrayOf(AppModule::class, RemoteModule::class, DatabaseModule::class, WeatherRepositoryModule::class))
@Singleton
interface AppComponent {
    fun inject(mainActivity: MainActivity)

}