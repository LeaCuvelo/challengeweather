package com.cuvelo.findochallenge.di

import android.content.Context
import com.cuvelo.findochallenge.data.db.RoomClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideRoomDatabase(context: Context) = RoomClient.getInstance(context)
}