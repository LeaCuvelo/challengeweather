package com.cuvelo.findochallenge.viewmodel


import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.cuvelo.findochallenge.data.db.CityEntity
import com.cuvelo.findochallenge.data.repository.IWeatherRepository
import javax.inject.Inject

class MainViewModel @Inject constructor(private val weatherRepository: IWeatherRepository) : AndroidViewModel(Application()) {

    private var dataSource : LiveData<List<CityEntity>>

    init {
        dataSource = weatherRepository.getCities()
     }

    fun getWeather(cityName: String) = weatherRepository.getWeather(cityName)

    fun getCities() = dataSource

    fun addCity(cityName: String) = weatherRepository.addCity(cityName)

    fun deleteCity(cityName: String) = weatherRepository.deleteCity(cityName)

}