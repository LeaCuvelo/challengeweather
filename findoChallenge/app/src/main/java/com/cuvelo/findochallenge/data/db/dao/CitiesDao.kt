package com.cuvelo.findochallenge.data.db.dao

import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.room.*
import com.cuvelo.findochallenge.data.db.CityEntity
import com.cuvelo.findochallenge.data.db.RoomConfig

@Dao
interface CitiesDao {

    @Insert
    fun insertAll(cities: List<CityEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCity(city: CityEntity)

    @Query(RoomConfig.SELECT_CITIES)
    fun getAllCities(): LiveData<List<CityEntity>>

    @Delete
    fun deleteCity(city: CityEntity)

    class DeleteCityAsyncTask internal constructor(private val mAsyncTaskDao: CitiesDao, private val cityName: String) :
        AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg voids: Void): Void? {
            mAsyncTaskDao.deleteCity(city = CityEntity(cityName))
            return null
        }
    }
}