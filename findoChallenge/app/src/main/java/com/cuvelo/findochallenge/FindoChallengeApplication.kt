package com.cuvelo.findochallenge

import android.app.Application
import com.cuvelo.findochallenge.di.*

class FindoChallengeApplication : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initializeDagger()
    }

    fun initializeDagger() {

        appComponent = DaggerAppComponent
                        .builder()
                        .appModule(AppModule(this))
                        .databaseModule(DatabaseModule())
                        .remoteModule(RemoteModule()).build()

    }

}