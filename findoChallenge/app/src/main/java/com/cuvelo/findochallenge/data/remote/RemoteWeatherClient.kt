package com.cuvelo.findochallenge.data.remote

import javax.inject.Inject

class RemoteWeatherClient @Inject constructor(private val remoteWeatherService: RemoteWeatherService) {

    fun getWeatherByCity(cityName: String) = remoteWeatherService.requestWeatherForCity(cityName)


}