package com.cuvelo.findochallenge.di

import com.cuvelo.findochallenge.data.repository.IWeatherRepository
import com.cuvelo.findochallenge.data.repository.WeatherRepository
import dagger.Binds
import dagger.Module

@Module
abstract class WeatherRepositoryModule {
    @Binds
    abstract fun bindsWeatherRepository(weatherRepository: WeatherRepository): IWeatherRepository
}