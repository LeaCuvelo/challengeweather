package com.cuvelo.findochallenge.data.repository

import androidx.lifecycle.LiveData
import com.cuvelo.findochallenge.data.db.CityEntity
import com.cuvelo.findochallenge.data.db.dto.WeatherDTO
import io.reactivex.Flowable
import io.reactivex.Single

interface IWeatherRepository {

    fun addCity(cityName: String)

    fun getCities(): LiveData<List<CityEntity>>

    fun getWeather(cityName: String): Single<WeatherDTO>

    fun deleteCity(cityName: String)

}