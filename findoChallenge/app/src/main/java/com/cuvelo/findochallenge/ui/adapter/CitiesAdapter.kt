package com.cuvelo.findochallenge.ui.adapter

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cuvelo.findochallenge.R
import com.cuvelo.findochallenge.data.db.CityEntity
import com.cuvelo.findochallenge.ui.activity.MainActivity
import kotlinx.android.synthetic.main.city_item.view.*

class CitiesAdapter(private var context: Context) : RecyclerView.Adapter<CitiesAdapter.CityViewHolder>() {

    private var dataSet: List<CityEntity> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): CityViewHolder {
        return CityViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.city_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        var containerActivity = context as MainActivity

        holder.cityName.text = dataSet[position].cityName
        holder.itemContainer.setOnClickListener {
            containerActivity.performWeatherDetailsObserver(dataSet[position].cityName, false)
        }

        holder.itemContainer.setOnLongClickListener {
            holder.deleteButton.visibility = View.VISIBLE
            return@setOnLongClickListener true
        }

        holder.deleteButton.setOnClickListener {

            AlertDialog.Builder(context)
                .setMessage(context.getString(R.string.delete_message))
                .setTitle(context.getString(R.string.delete_title))
                .setPositiveButton(context.getString(R.string.delete_yes)){ _, _ ->
                    holder.deleteButton.visibility = View.GONE
                    containerActivity.deleteCity(dataSet[position].cityName)
                }
                .setNegativeButton(context.getString(R.string.delete_no)){ _, _ ->
                    holder.deleteButton.visibility = View.GONE
                }
                .create()
                .show()
        }

    }

    override fun getItemCount() = dataSet.size

    class CityViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val itemContainer = view.city_item_container
        val cityName = view.city_name_item_tv
        val deleteButton = view.delete_item
    }

    fun setCities(cities: List<CityEntity>){
        dataSet = cities
        notifyDataSetChanged()
    }

}