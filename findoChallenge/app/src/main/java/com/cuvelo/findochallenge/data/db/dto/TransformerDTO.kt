package com.cuvelo.findochallenge.data.db.dto

import com.cuvelo.findochallenge.data.remote.WeatherResponse
import com.google.android.gms.maps.model.LatLng

object TransformerDTO {

    fun transformToWeatherDetailsDTO(weatherResponse: WeatherResponse?): WeatherDTO {

        val city: String = weatherResponse!!.name
        val temperature: Double = weatherResponse.main.temp
        val pressure: Int = weatherResponse.main.pressure
        val humidity: Int = weatherResponse.main.humidity
        val maxTemp: Double = weatherResponse.main.temp_max
        val minTemp: Double = weatherResponse.main.temp_min
        val iconCode: String = weatherResponse.weather[0].icon
        val coordinate: LatLng = LatLng(weatherResponse.coord.lat, weatherResponse.coord.lon)


        return WeatherDTO(
            cityName = city,
            temperature = temperature,
            pressure = pressure,
            humidity = humidity,
            maxTemp =  maxTemp,
            minTemp = minTemp,
            iconCode = iconCode,
            coordinate = coordinate
        )
    }

}