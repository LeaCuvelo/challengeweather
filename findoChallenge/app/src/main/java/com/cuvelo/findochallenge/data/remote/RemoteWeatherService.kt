package com.cuvelo.findochallenge.data.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RemoteWeatherService {

    @GET("weather")
    fun requestWeatherForCity(
        @Query("q") cityName: String,
        @Query("appid") apiKey: String = FORECAST_API_KEY): Single<WeatherResponse>

}