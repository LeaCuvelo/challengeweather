package com.cuvelo.findochallenge.data.db.dto

import com.google.android.gms.maps.model.LatLng
import org.parceler.Parcel
import org.parceler.ParcelConstructor

@Parcel(Parcel.Serialization.BEAN)
data class WeatherDTO @ParcelConstructor constructor(val cityName: String,
                                                     val temperature: Double,
                                                     val pressure: Int,
                                                     val humidity: Int,
                                                     val maxTemp: Double,
                                                     val minTemp:Double,
                                                     val iconCode: String,
                                                     val coordinate: LatLng)