package com.cuvelo.findochallenge.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cuvelo.findochallenge.data.repository.IWeatherRepository
import javax.inject.Inject


class ViewModelFactory @Inject constructor(val weatherRepository: IWeatherRepository) : ViewModelProvider.Factory{

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(weatherRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }


}