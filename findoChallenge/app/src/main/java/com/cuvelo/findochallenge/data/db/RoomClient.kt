package com.cuvelo.findochallenge.data.db

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import android.content.Context
import com.cuvelo.findochallenge.data.db.dao.CitiesDao

@Database(entities = arrayOf(CityEntity::class), version = 1)
abstract class RoomClient : RoomDatabase(){

    abstract fun weatherSearchCitiesDao(): CitiesDao

    companion object {

        @Volatile private var INSTANCE: RoomClient? = null

        fun getInstance(context: Context): RoomClient =
            INSTANCE ?: synchronized(this){
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext,
                                 RoomClient::class.java,
                                 RoomConfig.DATABASE_WEATHER).build()
    }



}