package com.cuvelo.findochallenge.common

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.view.inputmethod.InputMethodManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.room.TypeConverter
import com.cuvelo.findochallenge.data.remote.BASE_FORECAST_API_ICON_URL
import java.util.*



inline fun convertKelvinToCelcius(toConvert: Double? ) = if(toConvert != null) toConvert - 273.15 else 0

fun hideKeyboard(mActivity: Activity){
    var imm = mActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
}

inline fun iconUrlConverter(iconCode: String) = BASE_FORECAST_API_ICON_URL + iconCode + ".png"

fun requestLocationPermission(activity: Activity, ACCESS_FINE_LOCATION_CODE: Int) {
    ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), ACCESS_FINE_LOCATION_CODE)
}

fun isLocationPermissionEnabled(context: Context): Boolean {
    return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
}


