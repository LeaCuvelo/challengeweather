package com.cuvelo.findochallenge.data.repository

import androidx.lifecycle.LiveData
import com.cuvelo.findochallenge.data.db.CityEntity
import com.cuvelo.findochallenge.data.db.RoomClient
import com.cuvelo.findochallenge.data.db.dto.TransformerDTO
import com.cuvelo.findochallenge.data.db.dto.WeatherDTO
import com.cuvelo.findochallenge.data.remote.RemoteWeatherClient
import com.cuvelo.findochallenge.data.remote.WeatherResponse
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton
import com.cuvelo.findochallenge.data.db.dao.CitiesDao


@Singleton
class WeatherRepository @Inject constructor(private val remoteWeatherClient: RemoteWeatherClient,
                         private val roomClient: RoomClient) : IWeatherRepository{


    override fun addCity(cityName: String) {
        Completable.fromCallable { roomClient.weatherSearchCitiesDao().insertCity(CityEntity(cityName = cityName)) }
            .subscribeOn(
            Schedulers.io()).subscribe()
    }

    override fun getCities(): LiveData<List<CityEntity>> {
        return roomClient.weatherSearchCitiesDao().getAllCities()
    }

    override fun getWeather(cityName: String): Single<WeatherDTO> {
        return remoteWeatherClient.getWeatherByCity(cityName)
            .map{ weatherResponse: WeatherResponse ->
                TransformerDTO.transformToWeatherDetailsDTO(weatherResponse)
            }
            .retry(1)
    }

    override fun deleteCity(cityName: String) {
        CitiesDao.DeleteCityAsyncTask(roomClient.weatherSearchCitiesDao(), cityName).execute()
    }


}

