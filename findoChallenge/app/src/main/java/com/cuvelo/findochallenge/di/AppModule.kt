package com.cuvelo.findochallenge.di

import android.content.Context
import com.cuvelo.findochallenge.FindoChallengeApplication
import com.cuvelo.findochallenge.data.repository.IWeatherRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val weatherApplication: FindoChallengeApplication) {

    @Provides
    @Singleton
    fun provideContext(): Context = weatherApplication

}