package com.cuvelo.findochallenge.data.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import java.util.*


@Entity(tableName = RoomConfig.TABLE_CITIES)
@TypeConverters(DateTypeConverter::class)
data class CityEntity(
    @PrimaryKey
    var cityName: String,
    val created_at: Date = Date(System.currentTimeMillis())

)

class DateTypeConverter {

    @TypeConverter
    fun toDate(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }

    @TypeConverter
    fun toLong(value: Date?): Long? {
        return value?.time!!.toLong()
    }
}