package com.cuvelo.findochallenge.ui.activity

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import androidx.lifecycle.ViewModelProviders
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.cuvelo.findochallenge.FindoChallengeApplication
import com.cuvelo.findochallenge.R
import com.cuvelo.findochallenge.common.*
import com.cuvelo.findochallenge.data.db.dto.WeatherDTO
import com.cuvelo.findochallenge.ui.adapter.CitiesAdapter
import com.cuvelo.findochallenge.viewmodel.MainViewModel
import com.cuvelo.findochallenge.viewmodel.ViewModelFactory
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.squareup.picasso.Picasso
import com.testfairy.TestFairy
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var viewModel: MainViewModel
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    lateinit var mMapView: MapView
    private lateinit var googleMap: GoogleMap
    private lateinit  var adapter: CitiesAdapter
    private var ACCESS_FINE_LOCATION_CODE_RESULT = 200

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        TestFairy.begin(this, "SDK-1VMOJkNu")

        FindoChallengeApplication.appComponent.inject(this)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)

        initViews(savedInstanceState)

        loadRecentCities()

    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }

    override fun onResume() {
        super.onResume()
        mMapView.onResume()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.my_weather_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId.equals(R.id.get_my_weather))
            getMyWeaher()
        return true
    }

    override fun onPause() {
        super.onPause()
        mMapView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mMapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMapView.onLowMemory()
    }

    private fun loadRecentCities() {
        viewModel.getCities()
            .observe(this,
                Observer {
                    if(it.size > 0){
                        adapter.setCities(it)
                        hideNoCitiesLabel()
                    }
                    else
                        showNoCitiesLabel()
                })
    }

    private fun saveCity(weatherResponse: WeatherDTO?) {
        if (!weatherResponse?.cityName.isNullOrBlank())
            viewModel.addCity(weatherResponse!!.cityName)
    }

    fun deleteCity(cityName: String){
        viewModel.deleteCity(cityName)
    }

    private fun initViews(savedInstanceState: Bundle?) {

        cities_list_recyclerview.layoutManager = LinearLayoutManager(this)
        cities_list_recyclerview.setHasFixedSize(true)

        adapter = CitiesAdapter(this)
        cities_list_recyclerview.adapter = adapter

        search_input.setOnEditorActionListener { _, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_DONE
                || actionId == EditorInfo.IME_ACTION_SEARCH
                || event.action == KeyEvent.ACTION_DOWN
                || event.action == KeyEvent.KEYCODE_ENTER){

                hideKeyboard(this)

                if(!search_input.text.isNullOrBlank())
                    performWeatherDetailsObserver(search_input.text.toString(), false)
                else
                    Toast.makeText(this, getString(R.string.complete_search_warning), Toast.LENGTH_LONG).show()
                true
            }
            else
                false
        }

        close_button.setOnClickListener{
            hideDetail()
        }

        mMapView = mapView

        mMapView.onCreate(savedInstanceState)

    }

    fun performWeatherDetailsObserver(searchedCityName: String, myCurrentLocation: Boolean): Disposable? {
        progressBar.visibility = View.VISIBLE
        progressBar.animate()
        return viewModel.getWeather(searchedCityName)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { weatherResponse: WeatherDTO? ->
                    progressBar.visibility = View.GONE

                    fillDetail(weatherResponse)
                    if (!myCurrentLocation)
                        saveCity(weatherResponse)
                },
                { throwable: Throwable? ->
                    progressBar.visibility = View.GONE

                    throwable?.printStackTrace()
                    Toast.makeText(this, getString(R.string.error_city_search), Toast.LENGTH_LONG).show()
                }
            )
    }

    private fun fillDetail(weatherResponse: WeatherDTO?){
        city_name_tv.text = weatherResponse?.cityName.toString()
        current_temp_tv.text =  getString(R.string.temperature_string_placeholder, String.format("%.0f", convertKelvinToCelcius(weatherResponse!!.temperature)))
        pressure_tv.text =  getString(R.string.pressure_string_placeholder, weatherResponse.pressure)
        humidity_tv.text =  getString(R.string.humidity_string_placeholder, weatherResponse.humidity, "%")
        temp_max_tv.text =  getString(R.string.temperature_max_string_placeholder, String.format("%.0f", convertKelvinToCelcius(weatherResponse.maxTemp)))
        temp_min_tv.text =  getString(R.string.temperature_min_string_placeholder, String.format("%.0f", convertKelvinToCelcius(weatherResponse.minTemp)))
        Picasso
            .get()
            .load(iconUrlConverter(weatherResponse.iconCode))
            .into(weather_icon)

        fillInMap(weatherResponse.coordinate)
        showDetailCard()
    }

    private fun fillInMap(coordinate: LatLng) {
        mMapView.getMapAsync ( OnMapReadyCallback{
                mMap ->

            googleMap = mMap

            googleMap.setIndoorEnabled(false)

            googleMap.clear()

            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinate,15f))

        } )
    }

    private fun showNoCitiesLabel() {
        no_recent_search_label.visibility = View.VISIBLE
        cities_list_recyclerview.visibility = View.GONE
    }

    private fun hideNoCitiesLabel(){
        no_recent_search_label.visibility = View.GONE
        cities_list_recyclerview.visibility = View.VISIBLE
    }

    private fun showDetailCard(){
        weather_detail_card_container.visibility = View.VISIBLE
    }

    private fun hideDetail(){
        weather_detail_card_container.visibility = View.GONE
    }

    @SuppressLint("MissingPermission")
    private fun getMyWeaher() {

        if(isLocationPermissionEnabled(this)){
            var fusedLocationProvider = LocationServices.getFusedLocationProviderClient(this)
            fusedLocationProvider.lastLocation.addOnSuccessListener {
                    location -> geoLocateByCoordinate(location.latitude, location.longitude)
            }
        }
        else{
            requestLocationPermission(this, ACCESS_FINE_LOCATION_CODE_RESULT)
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == ACCESS_FINE_LOCATION_CODE_RESULT ) {

            if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show()
                getMyWeaher()
            }
            else
                Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
        }
    }

    private fun geoLocateByCoordinate(latitude: Double, longitude: Double){
        var geocoder = Geocoder(this)
        var addressListResult = listOf<Address>()

        try {
            addressListResult = geocoder.getFromLocation(latitude, longitude, 1)
            performWeatherDetailsObserver(addressListResult[0].adminArea, true)
        }
        catch(e: IOException){
            Log.e("MainActivity", "IOException " + e.message)
        }


    }



}
